﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

//Man sollte so gut es geht vermeiden den System namespace zu nutzen, da er sehr groß ist und man im normalfall nur wenige
//klassen aus dem namespace benötigt.
//Man kann "aliase" erstellen, die dafür sorgen, dass in unserer File alle verweise auf die DateTime klasse sich eigentlich
//auf System.DateTime beziehen.

using DateTime  = System.DateTime;
using Console   = System.Console;
using Exception = System.Exception;

namespace MultiThreadingTest
{
    public class Program
    {
        public long CurrentTimeStamp
        {
            get
            {
                //um an den unix timestamp zu kommen hier ein workaround, indem man vom aktuellen datetime object
                //einfach das datetime objekt vom 1.1.70 abzieht und sich davon die Millisecond komponente holt
                return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            }
        }

        public const int NUM_THREADS = 4;

        public List<string> m_values = new List<string>();
        
        //Das lock object wird genutzt um mithilfe des lock keywords einen scope zu definieren, der jedem thread der versucht
        //den scope aufzurufen signalisiert, dass (wenn der fall) ein anderer thread diesen scope bereits ausführt.
        //Hinweis: Die ausführung des threads stopt solange am lock keyword bis der scope wieder frei ist! (der code sollte
        //auf jeden fall früher oder später ausgeführt werden)
        private object m_lock = new object();

        public void Run()
        {
            //Die Thread klasse ist der wrapper zum erstellen eines neuen Threads.
            //Der klasse kann im Konstruktor die vom Thread aufgerufene entry methode übergeben werden.
            //Mit der start methode wird der neue thread schließlich gestartet.
            Thread thread;
            for(int i = 0; i < NUM_THREADS; ++i)
            {
                thread = new Thread(RunThread);
                thread.Name = "Thread_" + i;
                thread.Start();
            }
        }

        //Unser entry point für unseren erstellten thread.
        //Ähnlich wie bei der Main() methode für unseren Main Thread gilt auch hier,
        //sobald die methode durchgelaufen ist wird der thread automatisch beendet.
        private void RunThread()
        {
            //Die DateTime klasse kann für alle möglichen arten der abfrage und bearbeitung von Daten und Uhrzeiten
            //genutzt werden. In diesem Fall holen wir uns über CurrentTimeStamp (since epoch)
            //Epoch ist hier der 1. Januar 1970, time since epoch bedeuted die anzahl an millisekunden ab diesem datum.
            long currentTime = CurrentTimeStamp;
            long totalTime = 0;

            try
            {
                while (true)
                {
                    //Jede sekunde wollen wir DoSomething() aufrufen
                    if (CurrentTimeStamp - currentTime >= 1000)
                    {
                        DoSomething();
                        totalTime += 1000;
                        currentTime = CurrentTimeStamp;
                    }
                    //Nach 5 sekunden wollen wir den thread beenden.
                    if (totalTime >= 10000)
                    {
                        PrintAll();
                        break;
                    }
                }
            }
            catch (Exception _e)
            {
                Console.WriteLine("Error in Thread {0} occured{1}: {2}", Thread.CurrentThread.Name, _e.GetType().Name, _e.Message);
            }
        }

        private void DoSomething()
        {
            lock (m_lock)
            {
                m_values.Add("bla");
                //m_values.Add("blubb");
                //m_values.Add("test");
                //m_values.Add("foo");
                //m_values.Add("bar");
            }
        }

        private void PrintAll()
        {
            lock (m_lock)
            {
                foreach (var value in m_values)
                {
                    Console.WriteLine(value + " from " + Thread.CurrentThread.Name);
                }
            }
        }

        public static void Main(string[] args)
        {
            try
            {
                new Program().Run();
            }
            catch (Exception _e)
            {
                Console.WriteLine("Error occured{0}: {1}", _e.GetType().Name, _e.Message);
            }
            Console.ReadKey();
        }
    }
}

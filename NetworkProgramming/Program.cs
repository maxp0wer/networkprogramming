﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

/*
 * Netzwerke bestehen immer aus mehreren Rechnerverbünde die über Router miteinander verbunden sind,
 * da es keinen Sinn macht alle Rechner unmittelbar miteinander zu verbinden.
 * 
 * Kommunikationsweg/Datenstrom ist genormt und in Modellen beschrieben.
 * DoD-Modell->4 Schichten (layers) arbeitet. Wurde ursprünglich vom amerikanischen Verteidigunsministerium
 * entwickelt.
 * Später wurde das OSI-Modell entwickelt, dass das DoD Modell um 3 weitere Schichten ergänzt.
 * 
 * Jede dieser Schichten ist für bestimmte Problemlösungen ausgelegt.
 * Innerhalb der Schichten sorgen protokolle für eine einheitliche kommunikation.
 * 
 * Die Schichten sind von unten nach oben nach ihrer Abstraktionsschicht gestaffelt.
 * Beginnend bei der physischen übertragung bis hin zu abstrakten anwendungen.
 * 
 * Zwei wichtige Schichten sind zum einen 
 * - die Vermittlungsschicht:
 *      Diese sorgt innerhalb des Netzwerkes für die "Wegfindung" und fügt Daten zur Wegfindung dem Header hinzu.
 *      Das relevante Protokoll in dieser Schicht ist IP (Internet Protocoll)
 * - zum anderen die Transportschicht:
 *      Hier gibt es zwei wichtige Protokolle, die sich auch merklich in ihrem Verhalten unterscheiden.
 *      TCP - Transmission Control Protocol 
 *      TCP ist die "sichere" protokolvariante. Sie sorgt dafür, dass Pakete sicher und effektiv an ihrem Ziel ankommen.
 *      Sorgt dafür, dass doppelte Pakete verworfen werden und verlorene Pakete erneut gesendet werden.
 *      Wenn es aussschlaggebend ist, dass Pakete verlässlich und in der selben reihenfolge ankommen und geschwindigkeit
 *      nicht das a und o ist.
 *      UDP - User Datagram Protocol
 *      Weniger verlässlich wie das TCP Protokoll allerdings mit weniger overhead behaftet.
 *      Ist nicht garantiert, dass Pakete überhaupt ankommen. Es ist sogar möglich Daten ohne bestimmten
 *      Empfänger zu versenden.
 *      
 * Overhead: Ist all das an Daten, die nur zur Übertragung benötigt werden.
 * 
 * Addressen oder Zielangaben werden im Netzwerk über IP-Addressen gelöst.
 * Hier gab es ursprünglich nur die IPv4 Addressen, diese sind 32 bit lang und bestehen 4 zahlenfolgen, die 
 * durch punkte getrennt werden. Durch IPv4 können 4.294.967.296 verschiedene Addressen dargestellt werden.
 * Da das mitlerweile nicht mehr ausreicht wurde IPv6 entwickelt.
 * 
 * 127.0.0.1 - "localhost"
 * 0.0.0.0 - broadcast oder wildcard IP addresse
 * 
 * IPv6 kann eine viel größere Anzahl IP-Addressen darstellen, da eine IPv6 128 bit groß ist.
 * Besteht aus 8 zeichenketten (a 4 zeichen), die durch doppelpunkte getrennt sind.
 * 
 * Wenn Ip-Addressen die Hausnummer sind, sind Ports die Zimmernummer.
 * Sie verweisen auf bestimmte Dienste auf einer Maschine im Netzwerk.
 * Jeder Port kann nur einmalig für einen bestimmten Dienst reserviert werden,
 * bedeutet, dass nur ein Dienst/Programm o.ä. pro port angesprochen werden kann.
 * Innerhalb eines Programms kann nur ein Socket pro Port kommunizieren.
 * 
 * Es gibt bestimmte Standardports die bereits für bestimmte Anwendungen und Protokolle vorgesehen sind:
 * 80 oder 8080 für http dienste
 * 443 für https dienste
 * 21 ftp
 * 22 für ssh
 * 
 * DNS - Domain Name Systems
 * Da man lesbare adressen haben möchte und felxibler ip adressen wechseln möchte ist ein hostame die übliche
 * Art auf zielnetze zuzugreifen.
 * Der DNS-Server fungiert als Telefonbuch, indem pro name verschiedene IP-Addressen gespeichert sind.
 * Sobald man auf eine Internetseite zugreift verlangt der Router zunächst die Ip-Addressen zum hostname und verbindet
 * sich dann über diese.
 * 
 * 
 * */


namespace NetworkProgramming
{
    public class Program
    {
        public void Run()
        {
            string hostName = "";
            IPHostEntry hostEntry;

            Console.WriteLine("**** DNS Resolve Tool ****");
            do
            {
                try
                {
                    hostName = ReadHostName();
                    hostEntry = FetchHostInfo(hostName);
                    PrintHostEntry(hostEntry);
                }
                catch (Exception _e)
                {
                    Console.WriteLine("Unable to resolve host name: " + hostName);
                }
            } while (PromptForContinue("Do you want to try again"));
        }

        private string ReadHostName()
        {
            string value = "";

            while (string.IsNullOrWhiteSpace(value))
            {
                Console.WriteLine();
                Console.Write("Please enter the host name: ");
                value = Console.ReadLine();
            }

            return value;
        }

        private IPHostEntry FetchHostInfo(string _host)
        {
            return Dns.GetHostEntry(_host);
        }

        private void PrintHostEntry(IPHostEntry _entry)
        {
            Console.WriteLine("CName oder Host-Name: {0}", _entry.HostName);
            Console.WriteLine("Addresses:");
            for (int i = 0; i < _entry.AddressList.Length; ++i)
            {
                Console.WriteLine("\t- {0}", _entry.AddressList[i]);
            }
            Console.WriteLine("Aliases:");
            for (int i = 0; i < _entry.Aliases.Length; ++i)
            {
                Console.WriteLine("\t- {0}", _entry.Aliases[i]);
            }
        }

        private bool PromptForContinue(string _message)
        {
            string value = "";
            while (value != "y" && value != "x")
            {
                Console.WriteLine();
                Console.Write(_message + "(y/n)? ");
                value = Console.ReadLine();
            }
            return value == "y";
        }

        public static void Main(string[] args)
        {
            new Program().Run();
        }
    }
}

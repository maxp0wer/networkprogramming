﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protocol
{
    public static class Prot
    {
        public delegate void CommandHandler(Prot.Command _command);

        public static readonly int PORT = 25565;
        public static readonly int MAX_BUFFSIZE = 64;

        public static readonly string CMD_DELIMITER = "|";

        public static readonly char ARG_PREFIX = '?';
        public static readonly char ARG_SEPARATOR = '&';
        public static readonly char ARG_INITIALIZER = '=';

        public static readonly string CMD_EXIT = "exit";
        public static readonly string CMD_USRMSG = "usrmsg";
        public static readonly string CMD_CHANGEUSRNAME = "chngusrname";

        public static readonly string ARG_USRMSG_USR = "usr";
        public static readonly string ARG_USRMSG_MSG = "msg";

        public static readonly string ARG_CHANGEUSRNAME_OLD = "old";
        public static readonly string ARG_CHANGEUSRNAME_NEW = "new";

        //Wir bauen unser protokoll ähnlich wie get parameter bei http requests.
        //command?key=value&key=value

        public class Command
        {
            public string Name { get; private set; }
            public Dictionary<string, string> Args { get; private set; }

            public Command() : this("Unknown", null) { }

            public Command(string _name, Dictionary<string, string> _args)
            {
                Name = _name;
                Args = _args;
            }

            public static Command FromCommandString(string _commandString)
            {
                var cmdArgsSplit = _commandString.Split(ARG_PREFIX);
                var cmd = new Command();

                if (cmdArgsSplit.Length <= 0)
                {
                    cmd.Name = _commandString;
                    return cmd;
                }

                cmd.Name = cmdArgsSplit[0];

                if (cmdArgsSplit.Length == 1)
                {
                    return cmd;
                }

                if (cmdArgsSplit.Length > 2)
                {
                    throw new MalformedCommandException("Multiple command argument prefixes in message: " + _commandString);
                }

                cmd.Args = ParseCommandArgs(cmdArgsSplit[1]);

                return cmd;
            }

            public string ToCommandString()
            {
                return Name + GetArgsString() + CMD_DELIMITER;
            }

            private string GetArgsString()
            {
                string result = ARG_PREFIX.ToString();
                foreach (KeyValuePair<string, string> kvP in Args)
                {
                    result += kvP.Key + ARG_INITIALIZER + kvP.Value + ARG_SEPARATOR;
                }
                //Da unser argument string durch den code oben immer durch ein & abgeschlossen wird und
                //unsere splits beim parsen durch das & zeichen kaputt gehen. entfernen wir das letzte & zeichen
                //hier aus unserem resultat.
                result = result.Remove(result.LastIndexOf(ARG_SEPARATOR), 1);
                return result;
            }

            private static Dictionary<string, string> ParseCommandArgs(string _argumentString)
            {
                var result = new Dictionary<string, string>();
                //argumentstring: key=value&key=value&key=value
                var argSplit = _argumentString.Split(ARG_SEPARATOR);

                string[] keyValueSplit;
                foreach (string val in argSplit)
                {
                    //val: key=value
                    keyValueSplit = val.Split(ARG_INITIALIZER);
                    if (keyValueSplit.Length != 2)
                    {
                        throw new MalformedCommandException("Malformed argument key value pair: " + val);
                    }
                    if (result.ContainsKey(keyValueSplit[0]))
                    {
                        throw new MalformedCommandException("Multiple keys in argument list, key value: " + keyValueSplit[0]); 
                    }
                    result.Add(keyValueSplit[0], keyValueSplit[1]);
                }
                return result;
            }
        }

        
    }

    //Um sauber mit ausnahmen umzugehen bietet es sich an dieser stelle an eine exception zu werfen.
    //Dafür erstellen wir eine neue custom exception klasse.
    public class MalformedCommandException : System.Exception
    {
        public MalformedCommandException(string _message) : base(_message) { }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protocol
{
    public class NetworkListener
    {
        private Dictionary<string, Prot.CommandHandler> m_commandMap = new Dictionary<string, Prot.CommandHandler>();

        protected void CommandReceived(string _command)
        {
            Prot.Command cmd;
            try
            {
                cmd = Prot.Command.FromCommandString(_command);
            }
            catch (System.Exception _e)
            {
                System.Console.WriteLine("Error occurred while parsing command string({0}): {1}", _e.GetType(), _e.Message);
                return;
            }

            if (m_commandMap.ContainsKey(cmd.Name))
            {
                m_commandMap[cmd.Name](cmd);
                return;
            }

            System.Console.WriteLine("Invalid command received, dropping...");
            System.Console.WriteLine(_command);
        }

        protected void RegisterCommand(string _name, Prot.CommandHandler _callback)
        {
            if (m_commandMap.ContainsKey(_name))
                return;
            m_commandMap.Add(_name, _callback);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Net.Sockets;
using Protocol;

using Console       = System.Console;
using Exception     = System.Exception;
using Environment   = System.Environment;

/*
 * Socket engl für Steckdose
 * 
 * Ist die programmiertechnische schnittstelle zu Netzwerken.
 * Wenn der Applikationscode der Stecker ist, ist der Socket die dazugehörige Steckdose
 * über die wir Daten im Netzwerk senden/empfangen können.
 * 
 * Jeder socket wird direkt auf einen Port gebunden. Der Port ist danach für diesen Socket 
 * reserviert bis der darunterliegende Stream wieder geschlossen wird. 
 * 
 * Da es sich bei einem Socket letztlich um einen Datenstrom handelt, kann dieser genau 
 * wie ein FileStream beschrieben und gelesen werden.
 * */

namespace NetworkClient
{
    public class Client
    {
        private IPHostEntry m_hostInfo;
        private Socket m_clientSocket;

        private ServerListener m_serverListener;

        private string m_userName = "Guest";

        public void Run()
        {
            InitializeHostInfo();
            InitializeConnection();
            ReadConsoleInput();
            Quit();
        }

        public void ClientLostConnection(ServerListener _listener)
        {
            _listener.Shutdown();
            Quit();
        }

        private void InitializeHostInfo()
        {
            string hostName = "";

            while (true)
            {
                try
                {
                    hostName = ReadHostName();
                    m_hostInfo = FetchHostInfo(hostName);
                    if (m_hostInfo.AddressList.Length > 0)
                    {
                        Console.WriteLine("Hostname resolved to address: " + m_hostInfo.AddressList[0]);
                        break;
                    }
                }
                catch (Exception _e)
                {
                    Console.WriteLine("Unable to resolve host: " + hostName);
                }

                if (!PromptForContinue("Do you want to try again"))
                {
                    Quit();
                    break;
                }
            }
        }

        private void Send(Prot.Command _command)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(_command.ToCommandString());
                m_clientSocket.Send(data, 0, data.Length, SocketFlags.None);
            }
            catch (Exception _e)
            {
                Console.WriteLine("Client lost connection to server!");
                Quit();
            }
        }

        private void InitializeConnection()
        {
            try
            {
                //1. Addressfamily:
                //  - Addressierungsschema, das verwendet wird. z.B. ipv6, ipv4, unix
                //2. SocketType:
                //  - Hier nehmen wir für gewöhnlich den Stream-Socket. Mögliche andere Werte sind, 
                //  Raw-Socket oder Datagram-Socket, die aber nur selten verwendung finden.
                //3. Transportprotokol:
                //  - Mögliche werte sind hier z.B. TCP oder UDP
                m_clientSocket = new Socket(m_hostInfo.AddressList[0].AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                //Endpoint festlegen, dieser besteht aus der Addresse (Hausnummer) und dem Port (Zimmernummer).
                //An dieser Stelle nehmen wir der einfachheit halber den ersten eintrag unseres dns resolve.
                var ipEp = new IPEndPoint(m_hostInfo.AddressList[0], Prot.PORT);

                //Hier passiert die magie! Es wird eine Verbindung zum endpoint hergestellt
                //Die Connect() methode ist synchron, das bedeuted, dass die Ausführung des codes solange
                //am aufruf der methode stopt bis eine Verbindung hergestellt wurde.
                m_clientSocket.Connect(ipEp);

                //Mit dem Connected property kann überprüft werden ob eine verbindung besteht oder nicht.
                if (m_clientSocket.Connected)
                {
                    Console.WriteLine("Successfully connected to host: " + m_hostInfo.HostName);
                    m_serverListener = new ServerListener(this, m_clientSocket);
                }
            }
            catch (SocketException _e)
            {
                Console.WriteLine("Failed to connect to host ({0}): {1}", _e.ErrorCode, _e.Message);
            }
            catch
            {
                Console.WriteLine("An error has occurred while connecting to host!");
            }
        }

        private void Quit()
        {
            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();

            Console.WriteLine("Shutting down client socket...");
            if (m_clientSocket != null && m_clientSocket.Connected)
            {
                m_clientSocket.Shutdown(SocketShutdown.Both);
                m_clientSocket.Close();
            }
            Environment.Exit(0);
        }

        private IPHostEntry FetchHostInfo(string _host)
        {
            return Dns.GetHostEntry(_host);
        }

        private string ReadHostName()
        {
            string value = "";

            while (string.IsNullOrWhiteSpace(value))
            {
                Console.WriteLine();
                Console.Write("Please enter the host name: ");
                value = Console.ReadLine();
            }

            return value;
        }

        private void ReadConsoleInput()
        {
            string value = "";
            do {
                value = Console.ReadLine();
                //setname (Name)
                string[] command = value.Split(' ');

                if (command.Length <= 0)
                {
                    Console.WriteLine();
                    continue;
                }

                if (command[0] == "setname")
                {
                    if (command.Length != 2)
                    {
                        Console.WriteLine("Usage: setname (Name)");
                        continue;
                    }

                    Send(new Prot.Command(Prot.CMD_CHANGEUSRNAME,
                        new Dictionary<string, string>()
                        {
                            {Prot.ARG_CHANGEUSRNAME_OLD, m_userName},
                            {Prot.ARG_CHANGEUSRNAME_NEW, command[1]}
                        }));

                    m_userName = command[1];
                    continue;
                }

                Send(new Prot.Command(Prot.CMD_USRMSG, 
                    new Dictionary<string, string>()
                    {
                        {Prot.ARG_USRMSG_USR, m_userName},
                        {Prot.ARG_USRMSG_MSG, value}
                    }));
            } while (value != "exit");
        }

        private bool PromptForContinue(string _message)
        {
            string value = "";
            while (value != "y" && value != "x")
            {
                Console.WriteLine();
                Console.Write(_message + "(y/n)? ");
                value = Console.ReadLine();
            }
            return value == "y";
        }

        public static void Main(string[] args)
        {
            new Client().Run();
        }
    }
}

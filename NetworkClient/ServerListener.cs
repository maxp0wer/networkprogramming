﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using Protocol;
using System.Threading;

using Console = System.Console;
using Exception = System.Exception;

namespace NetworkClient
{
    public class ServerListener : NetworkListener
    {
        private bool m_isListening = true;

        private Client m_client;
        private Socket m_clientSocket;

        private Thread m_thread;

        public ServerListener(Client _client, Socket _clientSocket)
        {
            m_client = _client;
            m_clientSocket = _clientSocket;

            InitializeCommandMap();

            m_thread = new Thread(Run);
            m_thread.Start();

            Console.WriteLine("Server listener started!");
        }

        private void InitializeCommandMap()
        {
            RegisterCommand(Prot.CMD_USRMSG, OnUserMessageReceived);
            RegisterCommand(Prot.CMD_CHANGEUSRNAME, OnUserNameChanged);
        }

        public void Run()
        {
            try
            {
                while (m_isListening)
                {
                    byte[] bytesReceivedBuffer = new byte[Prot.MAX_BUFFSIZE];
                    int bytesReceived = 0;
                    string messageReceived = "";
                    int totalBytesReceived = 0;

                    while ((bytesReceived = m_clientSocket.Receive(bytesReceivedBuffer, 0, Prot.MAX_BUFFSIZE, SocketFlags.None)) > 0)
                    {
                        totalBytesReceived += bytesReceived;

                        //da nicht gesagt ist, dass wir die Menge "MAX_BUFFSIZE" an Daten im stream liegen haben.
                        //kann es sein das 0-bytes im array liegen. Diese wollen wir nicht im string haben und
                        //replacen deshalb
                        messageReceived += Encoding.UTF8.GetString(bytesReceivedBuffer).Replace("\0", "");

                        if (messageReceived.Contains(Prot.CMD_DELIMITER))
                        {
                            CommandReceived(messageReceived.Replace(Prot.CMD_DELIMITER, "").Trim());
                            break;
                        }
                    }

                    //Wenn wir keine daten mehr vom stream bekommen können wir aus der schleife breaken.
                    if (bytesReceived == 0)
                        break;
                }
            }
            catch (Exception _e)
            {
                Console.WriteLine("Failed to read message from server!");
                Console.WriteLine(_e.Message);
            }
            finally
            {
                if (m_clientSocket != null && m_isListening)
                {
                    m_client.ClientLostConnection(this);
                }
            }
        }

        public void Shutdown()
        {
            Console.WriteLine("Client lost connection to server!");
            m_isListening = false;
        }

        private void OnUserMessageReceived(Prot.Command _command)
        {
            Console.WriteLine("[{0}] {1}", 
                                _command.Args[Prot.ARG_USRMSG_USR], 
                                _command.Args[Prot.ARG_USRMSG_MSG]);
            Console.Beep();
        }

        private void OnUserNameChanged(Prot.Command _command)
        {
            Console.WriteLine("{0} is now known as {1}",
                                _command.Args[Prot.ARG_CHANGEUSRNAME_OLD],
                                _command.Args[Prot.ARG_CHANGEUSRNAME_NEW]);
        }
    }
}

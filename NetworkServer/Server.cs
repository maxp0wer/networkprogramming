﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocol;
using System.Net.Sockets;
using System.Net;

namespace NetworkServer
{
    public class Server
    {
        private const int MAX_PENDING_CONNECTIONS = 5;

        private bool m_isRunning = true;
        private Socket m_serverSocket;

        private List<ClientServant> m_servants = new List<ClientServant>();

        private object m_lock = new object();

        public void Run()
        {
            InitializeServerSocket();
            StartServer();
            Quit();
        }

        private void InitializeServerSocket()
        {
            try
            {
                //Socket initialisieren
                //AddressFamily.InterNetwork = ipv4
                m_serverSocket = new Socket(AddressFamily.InterNetworkV6, SocketType.Stream, ProtocolType.Tcp);
                //Socket auf port binden.
                //Beim IP endpoint können wir festlegen von welchen IP-Addressen aus verbindungen erlaubt sind.
                //0.0.0.0 -> alle IPv4-Addressen
                m_serverSocket.Bind(new IPEndPoint(IPAddress.IPv6Any, Prot.PORT));

                //socket starten und auf eingehende verbindungen warten.
                m_serverSocket.Listen(MAX_PENDING_CONNECTIONS);
            }
            catch (SocketException _e)
            {
                Console.WriteLine("Failed to initialize socket ({0}): {1}", _e.ErrorCode, _e.Message);
                Quit(_e.ErrorCode);
            }
        }

        private void StartServer()
        {
            Socket clientSocket = null;

            try
            {
                while (m_isRunning)
                {
                    Console.WriteLine("Awaiting client connections...");
                    //Die Socket.Accept() methode ist synchron, bedeutet, dass der die ausführung des codes solange 
                    //beim Aufruf abwartet bis ein client verbunden ist.
                    clientSocket = m_serverSocket.Accept();

                    IPEndPoint ep = clientSocket.RemoteEndPoint as IPEndPoint;
                    Console.WriteLine("Client connection incoming from {0}!", ep.Address);
                    
                    lock(m_lock)
                        m_servants.Add(new ClientServant(this, clientSocket));
                }
            }
            catch (Exception _e)
            {
                Console.WriteLine("Failed to handle client connection!");
                Console.WriteLine(_e.Message);
            }
        }

        public void ServantLostConnection(ClientServant _servant)
        {
            _servant.Shutdown();
            lock(m_lock)
                m_servants.Remove(_servant);
        }

        public void Broadcast(Prot.Command _command)
        {
            lock (m_lock)
            {
                foreach (var servant in m_servants)
                {
                    servant.Send(_command);
                }
            }
        }

        private void Quit(int _errCode = 0)
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            if (m_serverSocket != null)
            {
                //Close() an dieser stelle um alle ressourcen
                //und den port wieder freizugeben
                m_serverSocket.Close();
            }
            Environment.Exit(_errCode);
        }

        public static void Main(string[] args)
        {
            new Server().Run();
        }
    }
}

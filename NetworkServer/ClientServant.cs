﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using Protocol;

using Exception = System.Exception;
using Console   = System.Console;

namespace NetworkServer
{
    public class ClientServant : NetworkListener
    {
        private bool m_isListening;

        private Server m_server;
        private Socket m_clientSocket;

        private Thread m_thread;

        public ClientServant(Server _serverHandle, Socket _clientSocket)
        {
            m_isListening = true;

            m_server = _serverHandle;
            m_clientSocket = _clientSocket;

            InitializeCommandMap();

            m_thread = new Thread(Run);
            m_thread.Start();

            IPEndPoint ep = m_clientSocket.RemoteEndPoint as IPEndPoint;
            Console.WriteLine("Client Servant initialized, serving client @ {0}", ep.Address);
        }

        public void Run()
        {
            try
            {
                while (m_isListening)
                {
                    byte[] bytesReceivedBuffer = new byte[Prot.MAX_BUFFSIZE];
                    int bytesReceived = 0;
                    string messageReceived = "";
                    int totalBytesReceived = 0;

                    while ((bytesReceived = m_clientSocket.Receive(bytesReceivedBuffer, 0, Prot.MAX_BUFFSIZE, SocketFlags.None)) > 0)
                    {
                        totalBytesReceived += bytesReceived;

                        //da nicht gesagt ist, dass wir die Menge "MAX_BUFFSIZE" an Daten im stream liegen haben.
                        //kann es sein das 0-bytes im array liegen. Diese wollen wir nicht im string haben und
                        //replacen deshalb
                        messageReceived += Encoding.UTF8.GetString(bytesReceivedBuffer).Replace("\0", "");

                        if (messageReceived.Contains(Prot.CMD_DELIMITER))
                        {
                            CommandReceived(messageReceived.Replace(Prot.CMD_DELIMITER, "").Trim());
                            break;
                        }
                    }

                    //Wenn wir keine daten mehr vom stream bekommen können wir aus der schleife breaken.
                    if(bytesReceived == 0)
                        break;
                }
            }
            catch (Exception _e)
            {
                Console.WriteLine("Failed to read message from client!");
                Console.WriteLine(_e.Message);
            }
            finally
            {
                if (m_clientSocket != null && m_isListening)
                {
                    m_server.ServantLostConnection(this);
                }
            }
        }

        public void Send(Prot.Command _command)
        {
            try
            {
                byte[] data = Encoding.UTF8.GetBytes(_command.ToCommandString());
                m_clientSocket.Send(data, 0, data.Length, SocketFlags.None);
            }
            catch (Exception _e)
            {
                Console.WriteLine("Failed to send message to client!");
                Console.WriteLine(_e.Message);
                m_server.ServantLostConnection(this);
            }
        }

        public void Shutdown()
        {
            Console.WriteLine("Client lost connection! Shutting down socket...");
            //eventuell noch server shutdown command an den client
            m_clientSocket.Shutdown(SocketShutdown.Both);
            m_clientSocket.Close();
        }

        private void InitializeCommandMap()
        {
            RegisterCommand(Prot.CMD_EXIT, OnExitCommand);
            RegisterCommand(Prot.CMD_USRMSG, OnUserMessageReceived);
            RegisterCommand(Prot.CMD_CHANGEUSRNAME, OnUserNameChanged);
        }

        private void OnExitCommand(Prot.Command _command)
        {
            //Exit
        }

        private void OnUserMessageReceived(Prot.Command _command)
        {
            Console.WriteLine("User message from " + _command.Args[Prot.ARG_USRMSG_USR] + ": " + _command.Args[Prot.ARG_USRMSG_MSG]);
            m_server.Broadcast(_command);
        }

        private void OnUserNameChanged(Prot.Command _command)
        {
            Console.WriteLine("{0} is now known as {1}",
                                _command.Args[Prot.ARG_CHANGEUSRNAME_OLD],
                                _command.Args[Prot.ARG_CHANGEUSRNAME_NEW]);
            m_server.Broadcast(_command);
        }
    }
}
